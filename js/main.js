class Employee {
    constructor(name, age, salary){
        this._name = name;
        this._age = age;
        this._salary = salary
    }

    set name(newName) {
        this._name = newName
    }

    get name() {
        return this._name
    }

    set age(newAge) {
        if (newAge >= 18) {
            this._age = newAge
        } else {
            console.log("Вам менше 18 років.Доступу немає");
        }
    }

    get age() {
        return this._age
    }

    set salary(newSalary) {
        if (newSalary > 0) {
            this._salary = newSalary
        } else {
            console.log("Людина щось має заробляти!!!");
        }
    }

    get salary() {
        return this._salary
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang){
        super(name, age, salary);
        this.lang = lang
    }

    get salary() {
        return this._salary * 3
    }
}

const employee1 = new Employee("Oleksandr", 20, 25000)
const employee2 = new Employee("Pavlo", 26, 27000)
const employee3 = new Employee("Mark", 30, 30000)

console.log(employee1);
console.log(employee2);
console.log(employee3);



// employee1.age = 15
// console.log(employee1.age);
// employee1.salary = -20000
// console.log(employee1.salary);

console.log(employee1.name);
console.log(employee1.age);
console.log(employee1.salary);

console.log(`${employee1.name} заробляє ${employee1.salary} у свої ${employee1.age}`);

const programmer1 = new Programmer("Yan", 20, 25000, ["English", "Ukrainian"]);
const programmer2 = new Programmer("Inna", 28, 23000, ["Spain", "Portugal"])


console.log(programmer1);
console.log(programmer2);
console.log(programmer1.name);
console.log(programmer1.age);
console.log(programmer1.salary);
console.log(programmer1.lang.join(", "));